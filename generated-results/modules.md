| # | group id | artifact id | version | date | module name |
|---|----------|-------------|---------|------|-------------|
| 1 | ch.qos.logback | logback-classic | 1.3.0-alpha4 | 2018-02-11 | ch.qos.logback.classic |
| 2 | ch.qos.logback | logback-core | 1.3.0-alpha4 | 2018-02-11 | ch.qos.logback.core |
| 3 | com.github.marschall | memoryfilesystem | 1.2.1 | 2018-04-01 | com.github.marschall.memoryfilesystem |
| 4 | com.headius | invokebinder | 1.11 | 2018-01-02 | com.headius.invokebinder |
| 5 | javax.json.bind | javax.json.bind-api | 1.0 | 2017-06-19 | java.json.bind |
| 6 | javax.json | javax.json-api | 1.1.2 | 2017-11-02 | java.json |
| 7 | javax.money | money-api | 1.0.3 | 2018-04-16 | java.money |
| 8 | javax.xml.bind | jaxb-api | 2.3.0 | 2017-07-31 | java.xml.bind |
| 9 | javax.xml.soap | javax.xml.soap-api | 1.4.0 | 2017-06-13 | java.xml.soap |
| 10 | javax.xml.ws | jaxws-api | 2.3.0 | 2017-06-13 | java.xml.ws |
| 11 | org.apache.logging.log4j | log4j-api | 2.11.0 | 2018-03-11 | org.apache.logging.log4j |
| 12 | org.cryptomator | siv-mode | 1.3.0 | 2018-03-13 | org.cryptomator.siv |
| 13 | org.eclipse | yasson | 1.0.1 | 2017-11-09 | org.eclipse.yasson |
| 14 | org.glassfish | javax.json | 1.1.2 | 2017-11-02 | org.glassfish.java.json |
| 15 | org.javamoney.moneta | moneta-convert | 1.2.1 | 2018-04-16 | org.javamoney.moneta.convert |
| 16 | org.javamoney.moneta | moneta-convert-ecb | 1.2.1 | 2018-04-16 | org.javamoney.moneta.convert.ecb |
| 17 | org.javamoney.moneta | moneta-convert-imf | 1.2.1 | 2018-04-16 | org.javamoney.moneta.convert.imf |
| 18 | org.javamoney.moneta | moneta-core | 1.2.1 | 2018-04-16 | org.javamoney.moneta |
| 19 | org.jboss.spec.javax.transaction | jboss-transaction-api_1.2_spec | 2.0.0.Alpha1 | 2017-10-19 | java.transaction |
| 20 | org.jfxtras | jfxtras-agenda | 9.0-r1 | 2018-04-05 | jfxtras.agenda |
| 21 | org.jfxtras | jfxtras-all | 9.0-r1 | 2018-04-05 | jfxtras.icalendaragenda |
| 22 | org.jfxtras | jfxtras-common | 9.0-r1 | 2018-04-05 | jfxtras.common |
| 23 | org.jfxtras | jfxtras-controls | 9.0-r1 | 2018-04-05 | jfxtras.controls |
| 24 | org.jfxtras | jfxtras-font-roboto | 9.0-r1 | 2018-04-05 | jfxtras.font.roboto |
| 25 | org.jfxtras | jfxtras-fxml | 9.0-r1 | 2018-04-05 | jfxtras.fxml |
| 26 | org.jfxtras | jfxtras-gauge-linear | 9.0-r1 | 2018-04-05 | jfxtras.gauge.linear |
| 27 | org.jfxtras | jfxtras-icalendaragenda | 9.0-r1 | 2018-04-05 | jfxtras.icalendaragenda |
| 28 | org.jfxtras | jfxtras-icalendarfx | 9.0-r1 | 2018-04-05 | jfxtras.icalendarfx |
| 29 | org.jfxtras | jfxtras-menu | 9.0-r1 | 2018-04-05 | jfxtras.menu |
| 30 | org.jfxtras | jfxtras-window | 9.0-r1 | 2018-04-05 | jfxtras.window |
| 31 | org.joda | joda-beans | 2.2.1 | 2018-05-08 | org.joda.beans |
| 32 | org.joda | joda-collect | 1.0.1 | 2018-05-09 | org.joda.collect |
| 33 | org.joda | joda-convert | 2.0.1 | 2018-03-12 | org.joda.convert |
| 34 | org.jruby | jruby-complete | 9.1.17.0 | 2018-04-20 | com.headius.invokebinder |
| 35 | org.lwjgl | lwjgl | 3.1.6 | 2018-02-04 | org.lwjgl |
| 36 | org.lwjgl | lwjgl-assimp | 3.1.6 | 2018-02-04 | org.lwjgl.assimp |
| 37 | org.lwjgl | lwjgl-bgfx | 3.1.6 | 2018-02-04 | org.lwjgl.bgfx |
| 38 | org.lwjgl | lwjgl-egl | 3.1.6 | 2018-02-04 | org.lwjgl.egl |
| 39 | org.lwjgl | lwjgl-glfw | 3.1.6 | 2018-02-04 | org.lwjgl.glfw |
| 40 | org.lwjgl | lwjgl-jawt | 3.1.6 | 2018-02-04 | org.lwjgl.jawt |
| 41 | org.lwjgl | lwjgl-jemalloc | 3.1.6 | 2018-02-04 | org.lwjgl.jemalloc |
| 42 | org.lwjgl | lwjgl-lmdb | 3.1.6 | 2018-02-04 | org.lwjgl.lmdb |
| 43 | org.lwjgl | lwjgl-nanovg | 3.1.6 | 2018-02-04 | org.lwjgl.nanovg |
| 44 | org.lwjgl | lwjgl-nfd | 3.1.6 | 2018-02-04 | org.lwjgl.nfd |
| 45 | org.lwjgl | lwjgl-nuklear | 3.1.6 | 2018-02-04 | org.lwjgl.nuklear |
| 46 | org.lwjgl | lwjgl-openal | 3.1.6 | 2018-02-04 | org.lwjgl.openal |
| 47 | org.lwjgl | lwjgl-opencl | 3.1.6 | 2018-02-04 | org.lwjgl.opencl |
| 48 | org.lwjgl | lwjgl-opengl | 3.1.6 | 2018-02-04 | org.lwjgl.opengl |
| 49 | org.lwjgl | lwjgl-opengles | 3.1.6 | 2018-02-04 | org.lwjgl.opengles |
| 50 | org.lwjgl | lwjgl-ovr | 3.1.6 | 2018-02-04 | org.lwjgl.ovr |
| 51 | org.lwjgl | lwjgl-par | 3.1.6 | 2018-02-04 | org.lwjgl.par |
| 52 | org.lwjgl | lwjgl-sse | 3.1.6 | 2018-02-04 | org.lwjgl.sse |
| 53 | org.lwjgl | lwjgl-stb | 3.1.6 | 2018-02-04 | org.lwjgl.stb |
| 54 | org.lwjgl | lwjgl-tinyfd | 3.1.6 | 2018-02-04 | org.lwjgl.tinyfd |
| 55 | org.lwjgl | lwjgl-vulkan | 3.1.6 | 2018-02-04 | org.lwjgl.vulkan |
| 56 | org.lwjgl | lwjgl-xxhash | 3.1.6 | 2018-02-04 | org.lwjgl.xxhash |
| 57 | org.ow2.asm | asm | 6.1.1 | 2018-03-25 | org.objectweb.asm |
| 58 | org.ow2.asm | asm-all | 6.0_BETA | 2017-07-17 | org.objectweb.asm.all |
| 59 | org.ow2.asm | asm-analysis | 6.1.1 | 2018-03-25 | org.objectweb.asm.tree.analysis |
| 60 | org.ow2.asm | asm-commons | 6.1.1 | 2018-03-25 | org.objectweb.asm.commons |
| 61 | org.ow2.asm | asm-debug-all | 6.0_BETA | 2017-07-17 | org.objectweb.asm.all.debug |
| 62 | org.ow2.asm | asm-tree | 6.1.1 | 2018-03-25 | org.objectweb.asm.tree |
| 63 | org.ow2.asm | asm-util | 6.1.1 | 2018-03-25 | org.objectweb.asm.util |
| 64 | org.ow2.asm | asm-xml | 6.1.1 | 2018-03-25 | org.objectweb.asm.xml |
| 65 | org.slf4j | jcl-over-slf4j | 1.8.0-beta2 | 2018-03-21 | org.apache.commons.logging |
| 66 | org.slf4j | log4j-over-slf4j | 1.8.0-beta2 | 2018-03-21 | log4j |
| 67 | org.slf4j | slf4j-api | 1.8.0-beta2 | 2018-03-21 | org.slf4j |
| 68 | org.slf4j | slf4j-jdk14 | 1.8.0-beta2 | 2018-03-21 | org.slf4j.jul |
| 69 | org.slf4j | slf4j-nop | 1.8.0-beta2 | 2018-03-21 | org.slf4j.nop |
| 70 | org.slf4j | slf4j-simple | 1.8.0-beta2 | 2018-03-21 | org.slf4j.simple |
